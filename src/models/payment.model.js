const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * @swagger
 * definitions:
 *  Payment:
 *    type: object
 *    properties:
 *      paymentMethod:
 *        type: string
 *      customerInformationId:
 *        type: string
 *      orderId:
 *        type: string
 */

const PaymentSchema = new Schema({
  paymentMethod: {
    type: String,
    required: [true, 'paymentMethod is required.'],
  },
  customerInformationId: {
    type: String,
    required: [true, 'customerInformationId is required.'],
  },
  orderId: {
    type: String,
    required: [true, 'orderId is required.'],
  },
});

const Payment = mongoose.model('payment', PaymentSchema);

module.exports = Payment;
