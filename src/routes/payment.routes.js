const express = require('express');
const routes = express.Router();

const Payment = require('../models/payment.model');
const messageSender = require('../eventhandlers/message.sender');

/**
 * @swagger
 * /api/payment-management/payments:
 *    get:
 *     tags:
 *       - payments
 *     description: Retrieving all payments.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Payments are returned.
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/Payment'
 *       400:
 *          description: Something went wrong.
 */
routes.get('/', (req, res) => {
  Payment.find({})
    .then(payment => {
      res.status(200).json(payment);
    })
    .catch(error => {
      console.log({ error: error });
      res.status(400).json({ error: 'Something went wrong.' });
    });
});

/**
 * @swagger
 * /api/payment-management/payments/{id}:
 *    get:
 *     tags:
 *       - payments
 *     description: Retrieving payment with given ID.
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: String ID of the payment.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Payments are returned.
 *          schema:
 *            $ref: '#/definitions/Payment'
 *       400:
 *          description: Something went wrong.
 */
routes.get('/:id', (req, res) => {
  const id = req.params.id;

  Payment.findById(id)
    .then(payment => res.status(200).json(payment))
    .catch(error => {
      console.log(error);
      res.status(400).json({ error: 'Something went wrong.' });
    });
});

/**
 * @swagger
 * /api/payment-management/payments:
 *    post:
 *     tags:
 *       - payments
 *     description: Creating new payment in payment database. Also adds the newly created payment to the message broker.
 *     parameters:
 *        - in: body
 *          name: body
 *          description: Create new payment object.
 *          schema:
 *            $ref: '#/definitions/Payment'
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Payment is saved in database and returned.
 *          schema:
 *            $ref: '#/definitions/Payment'
 *       400:
 *          description: Something went wrong.
 */
routes.post('/', (req, res) => {
  const payment = new Payment(req.body);

  payment
    .save()
    .then(payment => {
      messageSender.publish('PAYMENT_COMPLETED', payment);
      res.status(200).send(payment);
    })
    .catch(error => {
      console.log({ error: error });
      res.status(400).json({ error: 'Something went wrong.' });
    });
});

/**
 * @swagger
 * /api/payment-management/payments/{id}:
 *    delete:
 *     tags:
 *       - payments
 *     description: Delete payment with given ID from database.
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           $ref: '#/definitions/Payment'
 *         type: string
 *         required: true
 *         description: String ID of the payment.
 *     responses:
 *       200:
 *          description: Payment is deleted and returned.
 *          schema:
 *            $ref: '#/definitions/Payment'
 *       400:
 *          description: Something went wrong.
 */
routes.delete('/:id', (req, res) => {
  const id = req.params.id;

  Payment.findByIdAndDelete(id)
    .then(payment => res.status(200).send(payment))
    .catch(error => {
      console.log(error);
      res.status(400).json({ error: 'Something went wrong.' });
    });
});

module.exports = routes;
