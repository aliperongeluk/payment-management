const mongoose = require('mongoose');
const { dbUrl } = require('../environment/config/mongodb.config');

mongoose.Promise = global.Promise;

mongoose.connect(dbUrl, { useNewUrlParser: true });
const connection = mongoose.connection
  .once('open', () => {
    console.log('Connected to Mongo on ' + dbUrl);
  })
  .on('error', error => {
    console.warn('Warning', error.toString());
  });

module.exports = connection;
