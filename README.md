# template

## Steps to refactor template:

- Change/delete README.md file
- Change mongodb url and use it in mongo.js (mongo.connect)
- Change Jenkinsfile with the right containerName
- Expose other port in server.js (default 6969)
- Change Dockerfile (WORKDIR /template && EXPOSE 6969)
- Change routes etc. to domain
- Change .env file with the Mongo credentials
- When working with rabbitmq, make sure to change the queueName

## Contains startup for:

- basic server with express
- rabbitmq publisher route
- rabbitmq receiver
- some template routes with mongodb connection
- mongodb connection and models
- swagger documentation
- docker and jenkins files for automation

Start this microservice with:

- npm start
- node server.js

## Jenkins

Pushes to master will be pushed online to the production environment using Jenkins.
